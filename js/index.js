let ApiTester = new Vue({
    el: '#api-tester',
    data: {
        data: null
    },
    methods: {
        sendRequest(request, url, type){
            console.log(request, url)

            let requestUrl = `${this.data.schemes[0]}://${this.data.host}${this.data.basePath}${url}`;
            console.log(requestUrl)
            fetch(requestUrl, {
                method: type,
                headers: {
                    'Content-Type': 'application/json'
                },
            })
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                    console.log(data)
                });

        }
    },
    async created() {
        fetch('/swagger.json')
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                console.log(data)
                this.data = data;
            });
    }
});
